class JsonWebToken
  DEFAULT_EXPIRATION_MINUTES = 30.minutes

  class << self
    def encode(payload, exp = DEFAULT_EXPIRATION_MINUTES.from_now.to_i)
      # JWT expects expiration time in seconds
      token = JWT.encode(payload.merge(:exp => exp.to_i), Rails.application.credentials.jwt_secret)
      { :token => token, :expires => exp.to_i }
    end

    def decode(token)
      JWT.decode(token, Rails.application.credentials.jwt_secret).first.with_indifferent_access
    end
  end
end
