class CreateTickers < ActiveRecord::Migration[7.0]
  def change
    create_table :tickers do |t|
      t.belongs_to :user
      t.string :symbol,       :null => false
      t.string :description,  :null => false
      t.timestamps          :null => false
    end
  end
end
