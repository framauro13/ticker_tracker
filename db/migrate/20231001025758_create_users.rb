class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :username,         :null => false
      t.string :first_name,       :null => false
      t.string :middle_name
      t.string :last_name,        :null => false
      t.string :display_name,     :null => false
      t.string :email,            :null => false
      t.string :password_digest
      t.string :created_by
      t.string :updated_by

      t.timestamps :null => false
    end

    add_index :users, :username, :unique => true, :name => "username_idx"
  end
end
