require "json_web_token"

module Auth
  class AuthorizationController < ApplicationController
    before_action :validate_username, :validate_password, :validate_user, :only => :authorize

    def authorize
      if user.authenticate(params[:password])
        return render :json => JsonWebToken.encode(:user => { :id => user.id })
      end

      render :json => errors_response("invalid credentials"), :status => :unauthorized
    end

    private

    def user
      @user ||= User.find_by_username(params[:username])
    end

    def validate_user
      render :json => errors_response("invalid credentials"), :status => :unauthorized if user.blank?
    end

    def validate_username
      render :json => errors_response("username is required"), :status => :bad_request if params[:username].blank?
    end

    def validate_password
      render :json => errors_response("password is required"), :status => :bad_request if params[:password].blank?
    end
  end
end
