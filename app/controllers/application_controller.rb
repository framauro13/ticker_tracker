class ApplicationController < ActionController::API
  def authorize!
    header = request.headers["Authorization"]

    return render :nothing => true, :status => :unauthorized if header.blank?

    decoded_token = JsonWebToken.decode(header.gsub(/^Bearer /, ""))

    return render :nothing => true, :status => :unauthorized if decoded_token.blank?

    return render :nothing => true, :status => :unauthorized if decoded_token[:exp].to_i < Time.now.to_i

    @current_user_id = decoded_token[:user][:id]
  rescue JWT::VerificationError, JWT::DecodeError
    render :json => errors_response("invalid token"), :status => :unauthorized
  end

  rescue_from ActionController::RoutingError do |exception|
    render_error(exception.message, :status => :not_found)
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render_error(exception.message, :status => :not_found)
  end

  rescue_from StandardError do |exception|
    render_error(exception.message)
  end

  def current_user
    @current_user ||= User.find_by_id(@current_user_id)
  end

  def errors_response(errors)
    { :errors => Array(errors) }
  end

  def render_error(message, status: :internal_server_error)
    render :json => errors_response(message), :status => status
  end

  private

  def auth_token
    header = request.headers["Authorization"]

    return if header.blank?
    return unless header.starts_with?("Bearer ")

    header.split(" ").last
  end
end
