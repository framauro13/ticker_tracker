module Admin
  class UsersController < ApplicationController
    before_action :authorize!, :except => [:create]

    before_action :validate_user, :only => [:show, :update, :destroy]
    before_action :validate_username, :validate_display_name, :validate_password_fields, :only => :create

    def index
      render :json => users
    end

    def show
      render :json => user
    end

    def create
      render :json => User.create!(user_params), :status => :created
    end

    def update
      return render :json => user if user.update(user_params)

      render :json => errors_response(user.errors), :status => :unprocessable_entity
    end

    def destroy
      return render :nothing => true, :status => :no_content if user.destroy

      render :json => errors_response(user.errors), :status => :conflict
    end

    def current
      render :json => current_user
    end

    private

    def user
      @user ||= User.find_by_id(params[:id])
    end

    def username
      params[:username]
    end

    def password
      params[:password]
    end

    def password_confirmation
      params[:password_confirmation]
    end

    def user_params
      attributes = [
        :username,
        :display_name,
        :email,
        :password,
        :password_confirmation,
        :created_by,
        :updated_by
      ]
      params.permit(attributes)
    end

    def validate_user
      return if user.present?

      render :json => errors_response("unable to find user with id: #{params[:id]}"), :status => :not_found
    end

    def validate_username
      return render :json => errors_response("username is required"), :status => :bad_request if username.blank?

      return if User.find_by_username(username).blank?

      render :json => errors_response("#{username} is already used"), :status => :bad_request
    end

    def validate_display_name
      return if params[:display_name].present?

      render :json => errors_response("display name is required"), :status => :bad_request
    end

    def validate_password_fields
      return render :json => errors_response("password is required"), :status => :bad_request if password.blank?

      if password.length < 8
        return render :json => errors_response("password must be at least 8 characters"), :status => :bad_request
      end

      if password_confirmation.blank?
        return render :json => errors_response("password_confirmation is required"), :status => :bad_request
      end

      return if password == password_confirmation

      render :json => errors_response("password and password_confirmation must match"), :status => :bad_request
    end
  end
end
