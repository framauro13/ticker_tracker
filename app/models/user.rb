class User < ApplicationRecord
  has_secure_password

  validates :display_name, :presence => true
  validates :username,     :presence => true
  validates :email,        :presence => true

  has_many :tickers

  validates_uniqueness_of :username
  validates_length_of :email, :minimum => 8

  def as_json(options = {})
    attributes = {
      :only => [
        :id,
        :display_name,
        :email,
        :username,
        :created_by,
        :updated_by,
        :created_at,
        :updated_at
      ],
      :include => :tickers
    }.merge(options)
    super(attributes)
  end
end

