class Ticker < ApplicationRecord
  belongs_to :user

  validates :user, :presence => true
  validates :symbol, :presence => true
  validates :description, :presence => true

  validates_length_of :symbol, :maximum => 5

  def as_json(options = {})
    attributes = {
      :only => [
        :id,
        :symbol,
        :description,
        :created_by,
        :updated_by,
        :created_at,
        :updated_at
      ]
    }.merge(options)
    super(attributes)
  end
end
