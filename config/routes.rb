Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  CRUD = [:index, :show, :create, :update, :destroy]

  # Defines the root path route ("/")
  # root "articles#index"
  scope :module => :auth do
    # TODO: Consider making the auth service it's own microservice outside of this API.
    # TODO: Delete endpoint for revoking/invalidating tokens
    post :authorize, :action => :authorize, :controller => :authorization
  end

  scope :module => :admin do
    get :current_user, :controller => :users, :action => :current
    resources :users, :only => CRUD
  end
end
