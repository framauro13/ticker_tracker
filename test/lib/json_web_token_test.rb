require "test_helper"
require "json_web_token"

class JsonWebTokenTest < ActiveSupport::TestCase
  test "a payload can be properly signed" do
    payload = {
      :user => "Test User",
      :some_id => SecureRandom.uuid
    }

    token = JsonWebToken.encode(payload)

    assert token.key?(:token), "A hash with a 'token' key should have been present"
    assert token.key?(:expires), "An 'expires' field should have been present"

    decoded = JsonWebToken.decode(token[:token])

    assert_equal payload[:user], decoded[:user]
    assert_equal payload[:some_id], decoded[:some_id]
  end

  test "payloads expire 30 minutes from the time they were encoded" do
    payload = {
      :user => "Test User",
      :some_id => SecureRandom.uuid
    }

    freeze_time do
      half_hour_from_now_millis = (Time.now + 30.minutes).to_i

      token = JsonWebToken.encode(payload)

      assert_equal half_hour_from_now_millis, token[:expires]

      decoded = JsonWebToken.decode(token[:token])

      assert decoded.key?(:exp), "Payload should have had the 'exp' field set."
      assert_equal half_hour_from_now_millis, decoded[:exp]
    end
  end

  test "improperly signed tokens are rejected when decoding" do
    payload = {
      :user => "Test User",
      :some_id => SecureRandom.uuid,
      :exp => 5.minutes.from_now.to_i
    }
    bad_token = JWT.encode(payload, "some_forged_secret")

    assert_raises JWT::VerificationError do
      JsonWebToken.decode(bad_token)
    end
  end

  test "junk base64 data should be rejected" do
    assert_raises JWT::DecodeError do
      JsonWebToken.decode(SecureRandom.base64)
    end
  end
end
