require "test_helper"

class TickerTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "a ticker can be saved by a user" do
    attributes = {
      :display_name => "Arthur Morgan",
      :username => "test.user",
      :email => "arthur.morgan@vanderlynde.com",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account"
    }

    user = User.create!(attributes)
    assert user.present?, "A user was not created"

    Ticker.create!(:user => user, :symbol => "AAPL", :description => "Apple")

    user = User.find_by_id(user.id)
    assert user.present?, "couldn't find user with id: #{user.id}"
    assert_equal 1, user.tickers.size

    actual_ticker = user.tickers.first

    assert_equal "AAPL", actual_ticker.symbol
    assert_equal "Apple", actual_ticker.description
  end

  test "ticker json includes attributes" do
    attributes = {
      :display_name => "Arthur Morgan",
      :username => "test.user",
      :email => "arthur.morgan@vanderlynde.com",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account"
    }

    user = User.create!(attributes)
    assert user.present?, "A user was not created"

    freeze_time do
      ticker = Ticker.create!(:user => user, :symbol => "AAPL", :description => "Apple")

      output = ticker.as_json.with_indifferent_access

      assert_equal "AAPL", output[:symbol]
      assert_equal "Apple", output[:description]
      assert_equal Time.now, output[:created_at]
    end
  end
end
