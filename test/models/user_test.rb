require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "A user can be created" do
    attributes = {
      :display_name => "Arthur Morgan",
      :username => "test.user",
      :email => "arthur.morgan@vanderlynde.com",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account"
    }

    user = User.create!(attributes)
    assert user.present?, "A user was not created"
    assert user.id.present?, "The user model did not have an id"
    assert_equal attributes[:display_name], user.display_name
    assert user.password_digest.present?
    assert user.created_at.present?
    assert user.updated_at.present?
  end

  test "A user can be found by username" do
    attributes = {
      :display_name => "Arthur Morgan",
      :email => "arthur.morgan@vanderlynde.com",
      :username => "test.user",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account"
    }
    User.create!(attributes)

    assert User.find_by_username("test.user").present?, "Couldn't find user by username."
  end

  test "A user can be authenticated" do
    attributes = {
      :display_name => "Arthur Morgan",
      :email => "arthur.morgan@vanderlynde.com",
      :username => "test.user",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account"
    }
    User.create!(attributes)

    user = User.find_by_username(attributes[:username])
    assert user.authenticate(attributes[:password]), "User could not be authenticated with password."
  end

  test "A bad password won't authenticate" do
    attributes = {
      :display_name => "Arthur Morgan",
      :email => "arthur.morgan@vanderlynde.com",
      :username => "test.user",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account"
    }
    User.create!(attributes)

    user = User.find_by_username(attributes[:username])
    refute user.authenticate("some.junk.password"), "User should not have been authenticated with bad password."
  end

  test "A user should not be created if the password confirmation does not match the password" do
    attributes = {
      :display_name => "Arthur Morgan",
      :email => "arthur.morgan@vanderlynde.com",
      :username => "test.user",
      :password => "test.password",
      :password_confirmation => "something.different",
      :created_by => "test.run.account"
    }

    assert_raises { User.create!(attributes) }
  end

  test "json does not include any password information" do
    attributes = {
      :display_name => "Arthur Morgan",
      :email => "arthur.morgan@vanderlynde.com",
      :username => "test.user",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account"
    }
    user = User.create!(attributes)

    assert user.present?, "Wasn't able to create a user!"

    output = JSON.parse(user.to_json)

    [:password, :password_confirmation, :password_digest].each do |key|
      refute output.key?(key), "JSON output should not have included the key: #{key}"
    end
  end

  test "json includes any saved tickers" do
    attributes = {
      :display_name => "Arthur Morgan",
      :email => "arthur.morgan@vanderlynde.com",
      :username => "test.user",
      :password => "test.password",
      :password_confirmation => "test.password",
      :created_by => "test.run.account",
      :tickers => [
        Ticker.create(:symbol => "AAPL", :description => "Apple"),
        Ticker.create(:symbol => "MSFT", :description => "Microsoft")
      ]
    }
    user = User.create!(attributes)

    assert user.present?, "Wasn't able to create a user!"

    output = JSON.parse(user.to_json).with_indifferent_access

    assert output[:tickers].present?, "'tickers' was not present in the JSON output"
    assert_equal 2, output[:tickers].size
    assert output[:tickers].map { |ticker| ticker[:symbol] }.include?("AAPL")
    assert output[:tickers].map { |ticker| ticker[:symbol] }.include?("MSFT")
  end
end
