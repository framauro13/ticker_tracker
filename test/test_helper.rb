require "simplecov"
SimpleCov.start
SimpleCov.formatters = [SimpleCov::Formatter::HTMLFormatter]

ENV["RAILS_ENV"] ||= "test"

require_relative "../config/environment"
require "rails/test_help"
require "minitest/autorun"
require "mocha/minitest"

require_relative "../lib/json_web_token"

V1_URL = "/api/v1"

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors, with: :threads)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def create_bearer_token
    user = users(:test_user)
    token = JsonWebToken.encode(:user => { :id => user.id })[:token]
    "Bearer #{token}"
  end
  # Add more helper methods to be used by all tests here...
end
